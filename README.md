# Alurakut Base

Seja bem vindo ao projeto base do Alurakut !!! Passos fundamentais:
- Marque esse projeto com uma estrela
- Siga as instruções das aulas e conteúdo extra da Imersão React Next.js
- Faça o deploy na Vercel e compartilhe

![Capa do Projeto](https://gerador-de-imagens-omariosouto-alura-challenges.vercel.app/api/image-generator?url=https://alurakut-eta.vercel.app/)

## Como colocar o meu projeto na vitrine da imersão?
- Vá na aba "Sobre" ou "About" do seu projeto no menu lateral que fica na esquerda dentro do repositório no GitHub
- Adicione a tag "alurakut" e a tag "imersao-react"
- E pronto!

## Onde está o Layout base?
- [Link](https://www.figma.com/file/xHF0n0qxiE2rqjqAILiBUB/Alurakut?node-id=58%3A0)


# Contribuidores 

| Pessoa que criou o projeto |
| --- |
| <!-- CHANNEL_PROJECTS:START -->
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/alura-challenges/alurakut'/>](https://alurakut-eta.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/rafaspdev/imersao-alura-nextjs'/>](https://github.com/rafaspdev/imersao-alura-nextjs)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/raphaom35/Alurakut'/>](https://github.com/raphaom35/Alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/Cromaa/Alurakut'/>](https://github.com/Cromaa/Alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/joaovictordantasj/alurakut'/>](https://alurakut-lac.vercel.app/)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/RodrigoRVSN/alurakut'/>](https://yorkut-rodrigorvsn.vercel.app/)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/JoaoVictorSou/alurakut'/>](https://github.com/JoaoVictorSou/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/pablohen/alurakut'/>](https://github.com/pablohen/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/Andre-Rafael/alurakut'/>](https://github.com/Andre-Rafael/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/ptmarmello/Alurakut'/>](https://github.com/ptmarmello/Alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/Guilherme-tr/alurakut'/>](https://github.com/Guilherme-tr/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/sbrunettajr/Alurakut'/>](https://github.com/sbrunettajr/Alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/lucalexand/alurakut'/>](https://github.com/lucalexand/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/luizfverissimo/alurakut'/>](https://github.com/luizfverissimo/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/tamaracosta/alurakut'/>](https://github.com/tamaracosta/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/giovannamoeller/alurakut'/>](alurakut-rho.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/tpfrois/alurakut'/>](alurakut-lilac.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/mathwcruz/alurakut'/>](https://github.com/mathwcruz/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/GuiCoimbraDeveloper/alurakut'/>](https://github.com/GuiCoimbraDeveloper/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/Allysonfreitas210695/alurakut'/>](alurakut-deploy.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/zNexTage/alurakut'/>](alurakut-two.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/brnofranco/alurakut'/>](https://github.com/brnofranco/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/leonardogiagio/alurakut'/>](alurakut-virid.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/git-giovanna/alurakut'/>](https://github.com/git-giovanna/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/mjuliacsouza/imersaoAluraREACT'/>](alurakut-gamma.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/omariosouto/alurakut'/>](https://alurakut.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/mentalitydark/alurakut'/>](alurakut-beta.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/gustaferreira/alurakut'/>](alurakut-base-nine.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/nicolasaigner/alurakut'/>](alurakut-five.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/joaovitorcf97/alurakut'/>](alurakut-eight.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/ivan-mori/alurakut'/>](alurakut-inky.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/joseeduardorp/alurakut'/>](https://github.com/joseeduardorp/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/Nothin04/alurakut'/>](https://github.com/Nothin04/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/EduardoMaizo/alurakut'/>](https://github.com/EduardoMaizo/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/HiratsukaMi/alurakut'/>](https://github.com/HiratsukaMi/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/DiogoMalfatti/alurakut'/>](https://github.com/DiogoMalfatti/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/barbaraaliverti/alurakut'/>](https://github.com/barbaraaliverti/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/robsongomes/alurakut'/>](alurakut-seven.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/ffsf-filho/alurakut'/>](https://github.com/ffsf-filho/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/carlos09v/Alurakut'/>](https://github.com/carlos09v/Alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/joaom00/alurakut'/>](https://github.com/joaom00/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/Jottal/alurakut'/>](alurakut-azure.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/az3vedo/alurakut'/>](https://github.com/az3vedo/alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/omariosouto/alurakut-alpha'/>](alurakut.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/gustaferreira/alurakut-base'/>](https://github.com/gustaferreira/alurakut-base)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/omariosouto/alurakut-code'/>](https://github.com/omariosouto/alurakut-code)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/FelipeFerreiraSS/AluraKut'/>](https://github.com/FelipeFerreiraSS/AluraKut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/maiquitome/alurakut-web'/>](https://github.com/maiquitome/alurakut-web)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/JairNeto1/aluraKut'/>](https://github.com/JairNeto1/aluraKut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/perosa100/AluraKut-imers-o'/>](alura-kut-imers-o.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/gabriel-elesbao/imersao-react-Alurakut'/>](https://github.com/gabriel-elesbao/imersao-react-Alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/omariosouto/alurakut-base'/>](alurakut-base.vercel.app)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/malufell/imersao-react-alurakut'/>](https://github.com/malufell/imersao-react-alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/brunagil/react_project_alurakut'/>](https://github.com/brunagil/react_project_alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/frgiovanna/alurakut-imersao-react'/>](https://github.com/frgiovanna/alurakut-imersao-react)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/gusdepaula/imersao-react-alurakut'/>](https://github.com/gusdepaula/imersao-react-alurakut)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/danicaus/alura-imersao-react'/>](https://github.com/danicaus/alura-imersao-react)
| [<img width='500px' src='https://opengraph.githubassets.com/cf9f1db04b6e4e2b7a984902d69b889f717d09cb94b8b4296ffffc16d0c73120/vickyad/alura-react-immersion'/>](alurakut-fawn.vercel.app)<!-- CHANNEL_PROJECTS:END --> |

[![licence mit](https://img.shields.io/badge/licence-MIT-blue.svg?style=flat-square)](https://github.com/alura-challenges/alurakut-base/blob/master/LICENSE)
